# Easy Way

O Link para acesso ao Vídeo de Informações sobre o Projeto Integrador:

https://youtu.be/UjivxozPW4o


Integrantes:

Scrum Master:
Ana Amorim e 
Pedro Minicz

Time:
Adriano Ribeiro Martins,
Fernando Daniel Santos,
Isaque Costa Beirao,
Junio Luiz Sendreto dos Santos,
Paulo Vitor Lima e
Pedro Lucas dos Santos Rodrigues





1.	Estória do Usuário
O motorista está tendo problemas com as rotas e tempo, em casos de usuários que não avisam que irão utilizar a van, perdendo tempo e gerando gastos, ou em casos que os usuários avisam existe perda de tempo com mapeamento de novas rotas. Assim o motorista deseja um aplicativo que solucione esses problemas.


2.	Requisitos Funcionais
2.1 Motorista

Gerenciamento de Destino:
•	O motorista pode adicionar um destino;
•	O motorista pode alterar o destino; e
•	O motorista pode excluir um destino.
Gerenciamento de Passageiro:
•	O motorista pode excluir o passageiro;
•	O motorista pode adicionar destinos ao passageiro;
•	O motorista pode alterar o destino do passageiro; e
•	O motorista pode alterar o horário de chegada para cada passageiro.

2.2 Passageiro
•	O passageiro pode criar apenas um usuário;
•	O passageiro pode alterar alguns dados cadastrais; e
•	O passageiro deve enviar sua presença na ida e volta todos os dias; e
•	O passageiro pode mudar de ideia e enviar alteração sobre a utilização da van.

2.3 Aplicação
Rota:
•	O aplicativo deve sugerir as rotas a partir do ponto de saída;
•	O aplicativo deve organizar a ordem de busca de passageiros; e
•	O motorista pode alterar o horário de chegada para cada passageiro.
Hora Prevista:
•	O aplicativo deve informar para o passageiro a hora prevista de chegada na ida; e
•	O aplicativo deve informar para o passageiro a hora prevista de chegada na volta.

3.	Requisitos não funcionais

•	O Aplicativo deve ser feito com APP INVENTOR.
